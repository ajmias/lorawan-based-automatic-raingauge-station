/*
 * config.h
 *
 *  Created on: 30-Jul-2021
 *      Author: Ajmi
 *      @about A single config file to configure the lorawan parameters without editing the main files.
 */

#ifndef RAINGAUGE_STATION_CONFIG_H_
#define RAINGAUGE_STATION_CONFIG_H_


#ifndef OVER_THE_AIR_ACTIVATION
#define OVER_THE_AIR_ACTIVATION                            0
#endif


/************************************Device Key Configuration***************************************/

#define DEVICE_EUI 						    {0x3e ,0x09 ,0x76 ,0xff ,0x36 ,0x82 ,0xbf ,0x37}

#define JOIN_EUI                            {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}
#define OTAA_APP_KEY						{0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}


#if !OVER_THE_AIR_ACTIVATION

#define DEVICE_ADDRESS 						(uint32_t)0xfc009752
#define NETWORK_SESSION_KEY					{0x2F, 0xA8, 0x18, 0xC4, 0x99, 0x36, 0xB0, 0x42, 0xCA, 0x22, 0xBE, 0xC7, 0x33, 0x57, 0x9B, 0xA1}
#define APP_SESSION_KEY						{0xD0, 0x0E, 0xB5, 0x03, 0xF1, 0x04, 0x83, 0x5B, 0x72, 0xF7, 0x0D, 0x64, 0xD3, 0xB4, 0x1F, 0x2B}

#endif

/************************************Device Key Configuration***************************************/



/************************************Device Operation Configuration***************************************/

#define SEND_INTERVAL						120000              /*the application data transmission duty cycle*/

#define ADR_STATE 							LORAWAN_ADR_OFF	   /*LoRaWAN Adaptive Data Rate * @note Please note that when ADR is enabled the end-device should be static  check commissioning.h*/

#define DATA_RATE							DR_3

#define APP_PORT							2

#define CLASS_OPERATION						CLASS_A

#define UPLINK_MSG_STATE					LORAWAN_UNCONFIRMED_MSG

/************************************Device Operation Configuration***************************************/




#endif /* RAINGAUGE_STATION_CONFIG_H_ */
